# Solution of three code exercises

I use use React create App 

## Reactjs bug fixing exercise 

I re-write the code as project for improvement.

The initial problem for the exercise was the following code:

```jsx
/**
* This React class is intended to query an endpoint that will return an alphanumeric string, after clicking a button.
* This component is passed a prop "apiQueryDelay", which delays the endpoint request by N milliseconds. There is a 
* second button to disable this functionality and have the endpoint request run immediately after button click.
* This data is then to be displayed inside a simple container.
* The "queryAPI" XHR handler will return the endpoint response in the form of a Promise (such as axios, fetch).
* The response object will look like the following: {data: "A0B3HCJ"}
* The containing element ref isn't used, but should remain within the class.
* Please identify, correct and comment on any errors or bad practices you see in the React component class below.
* Additionally, please feel free to change the code style as you see fit.
* Please note - React version for this exercise is 15.5.4
*/

import React, Component from 'react';
import queryAPI from 'queryAPI';


class ShowResultsFromAPI extends Component() {
  constructor() {
    super(props);

    this.container = null;
  }

  onDisableDelay() {
    this.props.apiQueryDelay = 0;
  }

  click() {
    if (this.props.apiQueryDelay) {
      setTimeout(function() {
        this.fetchData();
      }, this.props.apiQueryDelay);
    }
  }

  fetchData() {
    queryAPI()
      .then(function(response) {
        if (response.data) {
          this.setState({
            data: response.data,
            error: false
          });
        }
      });
  }

  render() {
    return <div class="content-container" ref="container">
            {
              if (!!error) {
                <p>Sorry - there was an error with your request.</p>
              }
              else {
                <p>data</p>
              }
            }
          </div>
          <Button onClick={this.onDisableDelay.bind(this)}>Disable request delay</Button>
          <Button onClick={this.click.bind(this)}>Request data from endpoint</Button>
  }
}

ShowResultsFromAPI.displayName = {
  name: "ShowResultsFromAPI"
};
ShowResultsFromAPI.defaultProps = {
  apiQueryDelay: 0
};
ShowResultsFromAPI.propTypes = {
  apiQueryDelay: React.propTypes.number
};

export ContentContainer;
```

To execute the code:
					`npm install`  then
										`npm start`

### Modification of the orignial query

I adjust few things:

#### I did use React ^16.2.0:

When I start a project, I always use the latest version of the available libraries, unless it is an existing project.

#### I created simulation of queryAPI instead of an imported version of it

It generates data like `{data:" A0B3HCJ "}` 1 second later, to mock a real API call and be able to run the sample for real.

### Implementation Points

#### Babel eslint formating

The file was formatted with eslint for a nicer configuration that can be found in the `.eslintrc` file

#### Using the latest js features with babel

I always try to use the latest babel features ("react", "env", "stage-0") although not very used in this exercise.

#### Document name

I renamed  the original file "ReactComponentExercise.js"  to "QueryResultsFromAPI" .

#### Export correction

 I updated the name and exports it by default, as it should always be in the file,

#### Correction of Container Backup

Loading the container with the arrow function in the prop ref.

#### Destruction and data error

For use and display of data and errors, I save in local constants, then prints them correctly

#### Ternary style for display

I prefer ternary declarations.

#### apiQueryDelay state property

I think that the properties of the interior of a component should not be mutated, but they should always be input.

#### the `container` property does not need to be 'initialized in the constructor.

 We don't need to create null in the constructor.

#### Errors must always be boolean

!! We do it and we can control it.
I prefer to control the value so that it is always true or false, especially in such a context of controlled use.

#### Move buttons to container

Because React one must have a child node contained in a single DOM element.

#### the property object is received in parameter by The constructor receives in parameter

We can't call super with them.

#### onClick fixes

we never need to pass the `this` element to the functions we call if we use bind, the context gives to the called function, so we can only use it in the" linked "function.

The #### button is not a component

I just use it button in html` button`

#### the apiQueryDelay state property is now by constructor a

Make es6 style code.

#### Correction of the context in the click () function

problems with the arrow function in the `setTimeout`.
Added destructuring to access `apiQueryDelay`.


#### fetchData with async

 queryAPI uses a normal function, we have lost the scope of the class, so `this` is no longer the Component and does not have a` setState` method. I repaired with an arrow function:

`` `js
fetchData () {
queryAPI (). then (response => {
if (response.data) {
this.setState ({
data: response.data,
error: false,
});
}
});
}
`` ''

But because right now we have async / I wait I preferred them for the solution

#### propTypes with style

I add propTypes at the end of components after export

#### class ShowResultsFromAPI extends Component () ??

We extend components without executing them

#### Move from displayName to a class property

So as not to delete the code that was moving to the right place.

#### Adding a default `apiQueryDelay`

When loading `ShowResultsFromAPI` from the` EntryPoint` index, I added a `23` just to have something to disable by clicking on it.

#### If is already disabled (apiQueryDelay === 0), I disabled the botton


## Second Exercise: Object Oriented Design and implementation 

Please use your most proficient programming language to create object oriented design and use test driven development to implement classes and methods with appropriate data structure and test the code for the following scenario. Please add comments describing any assumptions you make:

* [x] There are Teachers
* [x] There are Students
* [x] Students are in classes that teachers teach
* [x] Teachers can create multiple quizzes with many questions (each question is multiple choice) Teachers can assign quizzes to students
* [x] Students solve/answer questions to complete the quiz, but they don't have to complete it at once. (Partial submissions can be made).
* [x] Quizzes need to get graded
* [x] For each teacher, they can calculate each student's total grade accumulated over a semester for their classes.

### Solution

I added the solution here, for simplicity.

I use the `src / college` folder to have both the courses and the tests.

Jest is used for testing.

`npm test` to run them.

  Identifier generation is just a good practice / tool that I like. It is why I use the identifier defined in the base class only for Classes and StudentResults,

the exercise being focused on object oriented and a TDD exercise more than a real solution which could be priced and implemented without changes in a real system.

I made an implementation based on the only really mutable data (answers in the Questions). Since I don't have enough data I just want to store the minimum that should be stored and do calculations from there, so the classes contain real logic.