import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import QueryResultsFromAPI from './QueryResultsFromAPI';
import registerServiceWorker from './ServicesWorker';

ReactDOM.render(<QueryResultsFromAPI apiQueryDelay={23} />, document.getElementById('root'));
registerServiceWorker();
