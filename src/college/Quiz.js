/**
* I can generate the `totalGrade` for all the quizzes of one student in a specific class (And we could create a new class each 6 months). To make the proper aggregation, I need a School object to then search all the classes (classRooms) of this teacher
* and make a accumulated grade for all their students.
*/
import Base from './Base';

export default class Quiz extends Base {
  constructor({ questions = [] } = {}) {
    super();
    if (!questions.length) {
      throw new Error('Is boring to create a quiz without questions');
    }

    this.questions = questions;
  }

  grade() {
    const numberOfQuestions = this.questions.length;
    const rightAnswers = this.questions.filter(q => q.isOk()).length;
    return Number.parseFloat((rightAnswers / numberOfQuestions * 100).toFixed(2));
  }
}
